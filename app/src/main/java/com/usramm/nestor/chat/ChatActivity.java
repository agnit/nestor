package com.usramm.nestor.chat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.usramm.nestor.NestorApplication;
import com.usramm.nestor.infra.NestorPreferences;
import com.usramm.nestor.R;
import com.usramm.nestor.api.Message;
import com.usramm.nestor.api.Nestor;
import com.usramm.nestor.api.NestorService;
import com.usramm.nestor.firebase.NestorMessagingService;
import com.usramm.nestor.infra.ui.BaseActivity;
import com.usramm.nestor.reminder.ReminderActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_ACTION;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_ACTION_ALERT;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_ACTION_NOTIFY;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_ACTION_RING;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_TEXT;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_TITLE;

public class ChatActivity extends BaseActivity {

    private static final String TAG = "ChatActivity";

    private static final int SPEECH_REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        ImageButton btnRecognizeSpeech = (ImageButton) findViewById(R.id.btnSpeak);
        btnRecognizeSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySpeechRecognizer();
            }
        });

        ImageButton btnSend = (ImageButton) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txtMyText = (EditText)findViewById(R.id.txtMyText);
                String text = txtMyText.getText().toString().trim();
                if (!text.isEmpty()) {
                    sendToServer(text);
                    txtMyText.setText(null);
                }
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleNestorReply(intent);
    }

    private boolean handleNestorReply(Intent intent) {
        String text = intent.getStringExtra(NESTOR_TEXT);
        String title = intent.getStringExtra(NESTOR_TITLE);
        String action = intent.getStringExtra(NESTOR_ACTION);
        boolean validMessage = false;
        if (text != null && !text.isEmpty()
                && title != null && !title.isEmpty()
                && action != null && !action.isEmpty()) {
            validMessage = true;

            String spokenText = String.format("Nestor : %s", text);
            displayMessage(spokenText);
            if (action.equals(NESTOR_ACTION_NOTIFY)) {
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                }
            } else if (action.equals(NESTOR_ACTION_RING)){
                intent.setClass(this, ReminderActivity.class);
                startActivity(intent);
            }
        }

        return validMessage;
    }

    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);

            String text = results.get(0);

            sendToServer(text);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleTextFromSpeech(String text) {
        EditText txtMyText = (EditText)findViewById(R.id.txtMyText);
        if (txtMyText != null ) {
            txtMyText.append(" " + text);
        }
    }

    private void sendToServer(String text) {
        String spokenText = String.format("You : %s", text);
        displayMessage(spokenText);

        NestorPreferences nestorPreferences = NestorPreferences.getPreferences();
        String instanceId = nestorPreferences.getInstanceId(null);
        if (instanceId != null && !instanceId.isEmpty()) {
            NestorService service = Nestor.getService();
            Call<Void> call = service.speak(new Message(text, instanceId));
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Log.d(TAG, "speak success");
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.e(TAG, "speak failure");
                }
            });
        }
    }

    private void displayMessage(String msg) {
        TextView txtRecognizedSpeech = (TextView) findViewById(R.id.txtRecognizedSpeech);
        if (txtRecognizedSpeech.length() > 0){
            txtRecognizedSpeech.append("\n\n");
        }
        txtRecognizedSpeech.append(msg);
    }
}
