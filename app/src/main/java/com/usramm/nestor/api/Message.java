package com.usramm.nestor.api;

/**
 * Created by agnitsarkar on 6/13/17.
 */

public class Message {
    private String utext;
    private String iid;

    public Message(String utext, String iid) {
        this.utext = utext;
        this.iid = iid;
    }

    public String getUtext() {
        return utext;
    }

    public String getIid() {
        return iid;
    }
}
