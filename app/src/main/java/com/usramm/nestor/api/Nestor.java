package com.usramm.nestor.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by agnitsarkar on 6/9/17.
 */

public class Nestor {

    private static NestorService service;

    public static NestorService getService() {
        synchronized(Nestor.class) {
            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://usramm.com/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                service = retrofit.create(NestorService.class);
            }
        }
        return service;
    }
}
