package com.usramm.nestor.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by agnitsarkar on 6/12/17.
 */

public interface NestorService {
    @POST("/register")
    Call<Void> registerHandset(@Body Registration registration);

    @POST("/spoke")
    Call<Void> speak(@Body Message message);
}
