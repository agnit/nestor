package com.usramm.nestor.api;

/**
 * Created by agnitsarkar on 6/12/17.
 */

public class Registration {
    public String emailID;
    public String instanceID;

    public Registration(String emailID, String instanceID) {
        this.emailID = emailID;
        this.instanceID = instanceID;
    }
}
