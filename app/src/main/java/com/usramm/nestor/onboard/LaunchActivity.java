package com.usramm.nestor.onboard;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.usramm.nestor.R;
import com.usramm.nestor.api.Nestor;
import com.usramm.nestor.api.NestorService;
import com.usramm.nestor.api.Registration;
import com.usramm.nestor.chat.ChatActivity;
import com.usramm.nestor.infra.NestorPreferences;
import com.usramm.nestor.infra.SystemManager;
import com.usramm.nestor.infra.permission.PermissionPresenter;
import com.usramm.nestor.infra.ui.BaseActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LaunchActivity extends BaseActivity implements PermissionPresenter.Handler{

    private static final String TAG = "LaunchActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (NestorPreferences.getPreferences().isUserRegistered()) {
            openChatWindow();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkPermission();
    }

    @Override
    public void onPermissionsGranted() {
        String email = SystemManager.getManager().fetchEmailAddress();
        if (email != null && !email.isEmpty()) {
            NestorPreferences nestorPreferences = NestorPreferences.getPreferences();
            nestorPreferences.setUserEmail(email);

            String instanceId = nestorPreferences.getInstanceId(null);
            if (instanceId != null && !instanceId.isEmpty()) {
                registerUser(email, instanceId);
            } else {
                Log.d(TAG, "No instance id");
            }
        } else {
            Log.d(TAG, "No email found");
        }
    }

    @Override
    public void onPermissionsDenied(boolean neverAskAgain) {
        checkPermission();
    }

    public void registerUser(@NonNull String email, @NonNull String instanceId) {
        NestorService service = Nestor.getService();
        Call<Void> call = service.registerHandset(new Registration(email, instanceId));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, "registration success");
                NestorPreferences.getPreferences().setUserRegistered(true);
                openChatWindow();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "registration failure");
            }
        });
    }

    private void openChatWindow() {
        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkPermission() {
        NestorPreferences nestorPreferences = NestorPreferences.getPreferences();
        String email = nestorPreferences.getUserEmail(null);
        if (email != null && !email.isEmpty()) {
            openChatWindow();
        } else {
            permissionPresenter = new PermissionPresenter.Builder()
                    .setPermissions(new String[]{Manifest.permission.GET_ACCOUNTS})
                    .setView(this)
                    .setAllowPermissionsDlgTitle(getString(R.string.grant_contact_permission_title))
                    .setAllowPermissionsDlgMessageTop(getString(R.string.grant_contact_permission_msg_top))
                    .setAllowPermissionsDlgMessageBottom(getString(R.string.grant_contact_permission_msg_bottom))
                    .showImage(true)
                    .showRationale(true)
                    .build();

            permissionPresenter.handlePermissions(this);
        }
    }
}
