package com.usramm.nestor.firebase;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.usramm.nestor.chat.ChatActivity;

import java.util.Map;

/**
 * Created by agnitsarkar on 5/6/17.
 */

public class NestorMessagingService extends FirebaseMessagingService {
    public static final String BROADCAST_ACTION = "com.usramm.nestor.Broadcast";
    public static final String NESTOR_TEXT = "nestor_reply";
    public static final String NESTOR_TITLE = "nestor_title";
    public static final String NESTOR_ACTION = "nestor_action";

    public static final String NESTOR_ACTION_NOTIFY = "notify";
    public static final String NESTOR_ACTION_ALERT = "alert";
    public static final String NESTOR_ACTION_RING = "ring";

    private static final String KEY_TEXT = "text";
    private static final String KEY_TITLE = "title";
    private static final String KEY_ACTION = "action";

    private static final String TAG = "MessagingService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            Map <String, String>dataMap = remoteMessage.getData();
            String msgText = dataMap.get(KEY_TEXT);
            String msgTitle = dataMap.get(KEY_TITLE);
            String msgAction = dataMap.get(KEY_ACTION);

            if (msgAction != null && !msgAction.isEmpty()
                    && msgText != null && !msgText.isEmpty()
                    && msgTitle != null && !msgTitle.isEmpty()){
                Intent intent = new Intent();
                intent.setAction(BROADCAST_ACTION);
                intent.putExtra(NESTOR_TEXT, msgText);
                intent.putExtra(NESTOR_TITLE, msgTitle);
                intent.putExtra(NESTOR_ACTION, msgAction);
                sendBroadcast(intent);
            }
        }
    }
}
