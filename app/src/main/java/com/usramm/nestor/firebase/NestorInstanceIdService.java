package com.usramm.nestor.firebase;


import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.usramm.nestor.infra.NestorPreferences;
import com.usramm.nestor.api.Nestor;
import com.usramm.nestor.api.NestorService;
import com.usramm.nestor.api.Registration;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by agnitsarkar on 5/6/17.
 */

public class NestorInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "IIDService";
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        NestorPreferences.getPreferences().setInstanceId(refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        String email = NestorPreferences.getPreferences().getUserEmail(null);
        if (email != null && !email.isEmpty()) {
            NestorService service = Nestor.getService();
            Call<Void> call = service.registerHandset(new Registration(email, refreshedToken));
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                    Log.d(TAG, "registration success");
                }

                @Override
                public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                    Log.e(TAG, "registration failure");
                }
            });
        }
    }
}
