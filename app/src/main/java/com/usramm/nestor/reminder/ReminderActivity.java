package com.usramm.nestor.reminder;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.usramm.nestor.R;

import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_ACTION;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_TEXT;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_TITLE;

public class ReminderActivity extends AppCompatActivity {

    private static final String TAG = "ReminderActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);

        Intent intent = getIntent();
        String text = intent.getStringExtra(NESTOR_TEXT);
        String title = intent.getStringExtra(NESTOR_TITLE);
        String action = intent.getStringExtra(NESTOR_ACTION);

        if (text != null && !text.isEmpty()
                && title != null && !title.isEmpty()
                && action != null && !action.isEmpty()) {
            showReminder(text, title);
            playCancellableRingtone();
        }
        // Set the window to always display on the forefront and to be opened when locked
        Window oWindow = this.getWindow();

        oWindow.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                + WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                + WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void showReminder(String reminder, String title) {
        TextView txtReminder = (TextView) findViewById(R.id.txtReminder);
        txtReminder.setText(reminder);
    }

    private void playCancellableRingtone(){
        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            final Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
            ringtone.play();

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ringtone.stop();
                    finish();
                }
            });
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }
}
