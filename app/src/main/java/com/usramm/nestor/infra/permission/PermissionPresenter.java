/*
 * *
 *  * Copyright (c) 2017
 *  * All rights reserved.
 *
 */

package com.usramm.nestor.infra.permission;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.TextView;

import com.usramm.nestor.R;
import com.usramm.nestor.infra.SystemManager;

import java.lang.ref.WeakReference;

/**
 * Created by agnitsarkar on 4/25/17.
 *
 * Presenter class for handling dynamic permissions. It interacts with the @link PermissionModel to
 * get/set permission status and interacts with the view using the Handler
 * To obtain an object of this class, use the Builder.
 */

public class PermissionPresenter {

    private static final int PERMISSION_RESULT = 101;
    private String[] mPermissions;
    private String mPermissionsDlgTitle;
    private String mPermissionsDlgMsgTop;
    private String mPermissionsDlgMsgBottom;
    private String mOpenSettingsDlgMsg;
    private boolean mShowRationale = true;
    private boolean mShowImage = false;
    private boolean mShowOpenSettingsDlg = true;
    private Handler mHandler;
    private PermissionModel mPermissionModel;
    private WeakReference<Activity> mViewActivity;
    private WeakReference<Fragment> mViewFragment;
    private WeakReference<android.support.v4.app.Fragment> mViewSupportFragment;
    private PermissionPresenter(@NonNull PermissionModel permissionModel) {
        mPermissionModel = permissionModel;
    }

    /**
     * Call this to initiate the permission handling process, after the PermissionPresenter has been
     * built with required parameters. Checks if the requesting entity has the required permissions
     * and perform necessary actions as required to request the permissions.
     *
     * @param handler defines the granted and denied actions, once the permissions are granted/denied
     */
    public void handlePermissions(@NonNull Handler handler) {

        mHandler = handler;

        boolean allPermissionsGranted = true;
        // Check if all required permissions are available
        for (String permission: mPermissions) {
            if (!mPermissionModel.isPermissionGranted(mViewActivity.get(), permission)){
                allPermissionsGranted = false;
                break;
            }
        }

        if (allPermissionsGranted) {
            permissionsGranted();
        } else {
            // Check if any of the permissions has been denied by user with "Never ask again"
            boolean neverAskAgain = false;
            for (String permission: mPermissions) {
                if (!mPermissionModel.isAskingAllowedForPermission(permission)){
                    neverAskAgain = true;
                    break;
                }
            }

            if (neverAskAgain && mShowOpenSettingsDlg) {
                //Show the go to settings dialog
                showOpenSettingsDlg();
            } else {
                // Request the user for permissions
                if (mShowRationale) {
                    showAllowPermissionsDlg();
                } else {
                    requestPermissions();
                }
            }
        }
    }

    /**
     * Shows the permission rationale to the user asking him to allow the succeeding permission
     * requests
     */
    private void showAllowPermissionsDlg() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mViewActivity.get())
                .setTitle(mPermissionsDlgTitle)
                .setPositiveButton(R.string.button_continue, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        requestPermissions();
                    }
                })
                .setNegativeButton(mViewActivity.get().getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        rationaleDialogCancelled();
                    }
                })
                .setCancelable(false);
        final AlertDialog dialog = builder.create();
        View dialogLayout = mViewActivity.get().getLayoutInflater().inflate(R.layout.dialog_request_permission, null);
        if (!mShowImage) {
            dialogLayout.findViewById(R.id.imageView).setVisibility(View.GONE);
        }
        TextView textView1 = (TextView) dialogLayout.findViewById(R.id.textViewMsgTop);
        textView1.setText(mPermissionsDlgMsgTop);
        TextView textView2 = (TextView) dialogLayout.findViewById(R.id.textViewMsgBottom);
        textView2.setText(mPermissionsDlgMsgBottom);
        dialog.setView(dialogLayout);
        dialog.show();
    }

    /**
     * Request the permissions from the user
     */
    private void requestPermissions() {
        if (mViewFragment != null && mViewFragment.get().isAdded()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mViewFragment.get().requestPermissions(mPermissions, PERMISSION_RESULT);
            }
        } else if (mViewSupportFragment != null && mViewSupportFragment.get().isAdded()) {
            mViewSupportFragment.get().requestPermissions(mPermissions, PERMISSION_RESULT);
        } else {
            ActivityCompat.requestPermissions(mViewActivity.get(),
                    mPermissions,
                    PERMISSION_RESULT);
        }
    }

    /**
     * Shows the Open App settings dialog to the user requesting him to change the permissions
     * from app settings
     */
    private void showOpenSettingsDlg() {
        Context context = mViewActivity.get();

        final AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(mPermissionsDlgTitle)
                .setMessage(mOpenSettingsDlgMsg)
                .setPositiveButton(R.string.button_open_app_settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SystemManager.getManager().openAppSettings();
                        permissionsDenied();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        permissionsDenied();
                        dialog.dismiss();
                    }
                });
        Dialog dialog = builder.create();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                rationaleDialogCancelled();
            }
        });
        dialog.show();
    }

    /**
     * This is to be called from the onRequestPermissionsResult() Override to handle trigger
     * necessary actions, granted/denied handlers based on the permissions result
     * @param requestCode the request code used to request the permissions
     * @param permissions the list of permissions requested
     * @param grantResults the list of results
     */
    public void handlePermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_RESULT) {
            boolean allPermissionsGranted = true;

            // If request is cancelled, the result arrays are empty.
            if (grantResults.length == permissions.length) {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        allPermissionsGranted = false;
                        break;
                    }
                }
            } else {
                allPermissionsGranted = false;
            }

            if (allPermissionsGranted) {
                permissionsGranted();
            } else {
                permissionsDenied();
            }
        }
    }

    /**
     * Internal handling for permissions granted case. Triggers the stored handler for permission
     * grant
     */
    private void permissionsGranted() {
        for (String permission: mPermissions) {
            mPermissionModel.allowAskingForPermission(permission);
        }

        /***************************************************************************
         * This is required to make sure that onPermissionsGranted() is called after
         * onRequestPermissionsResult() returns. Else if UI transition is done in
         * onPermissionsGranted(), the app will run into IllegalStateException
         ****************************************************************************/
        new android.os.Handler().post(new Runnable() {
            @Override
            public void run() {
                mHandler.onPermissionsGranted();
            }
        });
    }

    /**
     * Internal handling for permissions denied case. Triggers the stored handler for permission
     * denial
     */
    private void permissionsDenied() {
        boolean neverAskAgain = false;
        for (String permission: mPermissions) {
            // If the user has chosen "Never ask again" for a permission, remember the preference
            // Else clear the preference.
            // This is required to handle "Never ask again" cases, which may be cleared from the
            // App Settings -> Permissions
            if (mPermissionModel.isNeverAskAgainCheckedForPermission(mViewActivity.get(), permission)) {
                mPermissionModel.disallowAskingForPermission(permission);
                neverAskAgain = true;
            } else {
                mPermissionModel.allowAskingForPermission(permission);
            }
        }

        /***************************************************************************
         * This is required to make sure that onPermissionsDenied() is called after
         * onRequestPermissionsResult() returns. Else if UI transition is done in
         * onPermissionsDenied(), the app will run into IllegalStateException
         ****************************************************************************/
        final boolean tempNeverAskAgain = neverAskAgain;
        new android.os.Handler().post(new Runnable() {
            @Override
            public void run() {
                mHandler.onPermissionsDenied(tempNeverAskAgain);
            }
        });
    }

    /**
     * Internal handling for the rationale dialog cancellation by user. Triggers the stored handler
     * for permission denial
     */
    private void rationaleDialogCancelled() {
        boolean neverAskAgain = false;
        for (String permission : mPermissions) {
            if (!mPermissionModel.isAskingAllowedForPermission(permission)) {
                neverAskAgain = true;
                break;
            }
        }
        mHandler.onPermissionsDenied(neverAskAgain);
    }

    /**
     * Used to define the permission granted and denial handlers
     */
    public interface Handler {
        /**
         * This is called when permissions are granted
         */
        void onPermissionsGranted();

        /**
         * This is called when permissions are denied.
         * @param neverAskAgain true if "never ask again" has been checked on any permission
         */
        void onPermissionsDenied(boolean neverAskAgain);
    }

    /**
     * Builder class for PermissionPresenter
     */
    public static class Builder {

        private PermissionPresenter mPermissionPresenter;

        public Builder() {
            mPermissionPresenter = new PermissionPresenter(new PermissionModel());
        }

        /**
         * Builds the PermissionPresenter with parameters specified by setters
         * @return The PermissionPresenter instance
         */
        public PermissionPresenter build() {
            if (mPermissionPresenter.mViewFragment == null &&
                    mPermissionPresenter.mViewActivity == null &&
                    mPermissionPresenter.mViewSupportFragment == null) {
                throw new RuntimeException("Did you forget to set the view ? Use the appropriate setView() method to do so before calling build().");
            }
            return mPermissionPresenter;
        }

        /**
         * Sets the view as the activity passed
         * @param activity
         * @return the builder instance
         */
        public Builder setView(@NonNull Activity activity) {
            mPermissionPresenter.mViewActivity = new WeakReference<Activity>(activity);
            return this;
        }

        /**
         * Sets the view as the support fragment passed
         * @param fragment
         * @return the builder instance
         */
        public Builder setView(@NonNull Fragment fragment) {
            mPermissionPresenter.mViewFragment = new WeakReference<>(fragment);
            mPermissionPresenter.mViewActivity = new WeakReference<Activity>(fragment.getActivity());
            return this;
        }

        /**
         * Sets the view as the fragment passed
         * @param fragment
         * @return the builder instance
         */
        public Builder setView(@NonNull android.support.v4.app.Fragment fragment) {
            mPermissionPresenter.mViewSupportFragment = new WeakReference<>(fragment);
            mPermissionPresenter.mViewActivity = new WeakReference<Activity>(fragment.getActivity());
            return this;
        }

        /**
         * Sets the permissions to be requested
         * @param permissions
         * @return the builder instance
         */
        public Builder setPermissions(@NonNull String[] permissions) {
            mPermissionPresenter.mPermissions = permissions;
            return this;
        }

        /**
         * Sets the permission rationale dialog title
         * @param title
         * @return the builder instance
         */
        public Builder setAllowPermissionsDlgTitle(@NonNull String title) {
            mPermissionPresenter.mPermissionsDlgTitle = title;
            return this;
        }

        /**
         * Sets the permission rationale dialog top message
         * @param msg
         * @return the builder instance
         */
        public Builder setAllowPermissionsDlgMessageTop(@NonNull String msg) {
            mPermissionPresenter.mPermissionsDlgMsgTop = msg;
            return this;
        }

        /**
         * Sets the permission rationale dialog bottom message
         * @param msg
         * @return the builder instance
         */
        public Builder setAllowPermissionsDlgMessageBottom(@NonNull String msg) {
            mPermissionPresenter.mPermissionsDlgMsgBottom = msg;
            return this;
        }

        /**
         * Sets the open settings dialog top message
         * @param msg
         * @return the builder instance
         */
        public Builder setOpenSettingsMessage(@NonNull String msg) {
            mPermissionPresenter.mOpenSettingsDlgMsg = msg;
            return this;
        }

        /**
         * Sets the rationale dialog to be displayed. This dialog is displayed by default.
         * @param show
         * @return the builder instance
         */
        public Builder showRationale(boolean show) {
            mPermissionPresenter.mShowRationale = show;
            return this;
        }

        /**
         * Sets the rationale dialog image to be displayed. Image is NOT shown by default.
         * @param show
         * @return the builder instance
         */
        public Builder showImage(boolean show) {
            mPermissionPresenter.mShowImage = show;
            return this;
        }

        /**
         * Sets the open settings dialog to be displayed. This dialog is displayed by default.
         * @param show
         * @return the builder instance
         */
        public Builder showOpenSettingsDlg(boolean show) {
            mPermissionPresenter.mShowOpenSettingsDlg = show;
            return this;
        }
    }
}
