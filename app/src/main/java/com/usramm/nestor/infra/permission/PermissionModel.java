/*
 * *
 *  * Copyright (c) 2017
 *  * All rights reserved.
 *
 */

package com.usramm.nestor.infra.permission;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.usramm.nestor.infra.NestorPreferences;

/**
 * Created by agnitsarkar on 4/25/17.
 *
 * Model for accessing dynamic permission information
 */

public class PermissionModel {
    /**
     *
     * @param activity The activity checking the permission status
     * @param permission The permission name
     * @return true if permission is granted, false otherwise
     */
    public boolean isPermissionGranted(Activity activity, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED) {
            allowAskingForPermission(permission);
            return true;
        }
        return false;
    }

    /*
        Check if Never ask again is checked for given permission
        @param activity The activity that wants to check for permission. For a fragment, this is its parent activity
        @param permissionName The permission name to check for.
        @return boolean. Returns true only if the permission is denied and never ask again is checked by user OR
                         if the permission never asked for (which is a shortcoming of Android)
    */
    public boolean isNeverAskAgainCheckedForPermission(Activity activity, String permission) {
        if (!isPermissionGranted(activity, permission))
            return !ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
        return false;
    }

    /*
        Check if the permission should be asked again, i.e. if Never ask again is not checked for this permission
        @param permission The permission name to check for.
        @return boolean. Returns true if the permission can be asked again
    */
    public boolean isAskingAllowedForPermission(String permission) {
        return NestorPreferences.getPreferences().canAskPermission(permission);
    }

    /*
        Specify the permission not be asked again. Call this as a result of checking "Never ask again"
        @param permission The permission name
    */
    public void disallowAskingForPermission(String permission) {
        NestorPreferences.getPreferences().allowAskingPermission(permission, false);
    }

    /*
        Specify the permission can be asked again. Call this to clear "Never ask again" status
        @param permission The permission name
    */
    public void allowAskingForPermission(String permission) {
        NestorPreferences.getPreferences().allowAskingPermission(permission, true);
    }
}
