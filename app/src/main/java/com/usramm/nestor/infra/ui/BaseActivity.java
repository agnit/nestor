package com.usramm.nestor.infra.ui;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.usramm.nestor.NestorApplication;
import com.usramm.nestor.infra.permission.PermissionPresenter;

public class BaseActivity extends AppCompatActivity {

    protected PermissionPresenter permissionPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissionPresenter != null) {
            permissionPresenter.handlePermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((NestorApplication)getApplication()).activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ((NestorApplication)getApplication()).activityPaused();
    }
}
