package com.usramm.nestor.infra;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * Created by agnitsarkar on 7/11/17.
 */

public class SystemManager {
    private static SystemManager systemManager;
    private WeakReference<Context> context;

    public static void initSystemManager(@NonNull Context context) {
        if(systemManager == null){
            synchronized (NestorPreferences.class) {
                if(systemManager == null){
                    systemManager = new SystemManager(context);
                }
            }
        }
    }

    private SystemManager(@NonNull Context context) {
        this.context = new WeakReference<Context>(context);
    }

    public static SystemManager getManager() {
        return systemManager;
    }

    public void openAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", context.get().getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.get().startActivity(intent);
    }

    public String fetchEmailAddress() {
        AccountManager manager = AccountManager.get(context.get());
        Account[] list = manager.getAccounts();
        String gmail = null;

        for(Account account: list) {
            if(account.type.equalsIgnoreCase("com.google")) {
                gmail = account.name;
                break;
            }
        }
        return gmail;
    }
}
