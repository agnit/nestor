package com.usramm.nestor.infra;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;

/**
 * Created by agnitsarkar on 5/20/17.
 */

public class NestorPreferences {

    private static NestorPreferences nestorPreferences;
    private static String FILE_KEY = "com.usramm.nestor.nestorPreferences";
    private static String INSTANCE_ID = "INSTANCE_ID";
    private static String USER_EMAIL = "USER_EMAIL";
    private static String USER_REGISTERED = "USER_REGISTERED";

    private WeakReference<Context> context;

    public static void initPreferences(@NonNull Context context) {
        if(nestorPreferences == null){
            synchronized (NestorPreferences.class) {
                if(nestorPreferences == null){
                    nestorPreferences = new NestorPreferences(context);
                }
            }
        }
    }

    public static NestorPreferences getPreferences() {
        return nestorPreferences;
    }

    private NestorPreferences(@NonNull Context context) {
        this.context = new WeakReference<>(context);
    }

    public void setUserEmail(@NonNull String email) {
        putString(USER_EMAIL, email);
    }

    public void setInstanceId(@NonNull String instanceId) {
        putString(INSTANCE_ID, instanceId);
    }

    public String getUserEmail (String defaultValue) {
        return getString(USER_EMAIL, defaultValue);
    }

    public String getInstanceId (String defaultValue) {
        return getString(INSTANCE_ID, defaultValue);
    }

    public Boolean isUserRegistered () {
        return getBoolean(USER_REGISTERED, false);
    }

    public void setUserRegistered (boolean registered) {
         putBoolean(USER_REGISTERED, registered);
    }

    public Boolean canAskPermission (String permission) {
        return getBoolean(permission, true);
    }

    public void allowAskingPermission (String permission, Boolean value) {
        putBoolean(permission, value);
    }

    private void putString(String key, String value) {
        SharedPreferences sharedPreferences =  context.get().getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(key, value).apply();
    }

    private String getString(String key, String defaultValue) {
        SharedPreferences sharedPreferences =  context.get().getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, defaultValue);
    }

    private void putBoolean(String key, Boolean value) {
        SharedPreferences sharedPreferences =  context.get().getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    private Boolean getBoolean(String key, Boolean defaultValue) {
        SharedPreferences sharedPreferences =  context.get().getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, defaultValue);
    }
}
