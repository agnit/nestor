package com.usramm.nestor;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import android.support.multidex.MultiDexApplication;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.usramm.nestor.chat.ChatActivity;
import com.usramm.nestor.firebase.NestorMessagingService;
import com.usramm.nestor.infra.NestorPreferences;
import com.usramm.nestor.infra.SystemManager;
import com.usramm.nestor.reminder.ReminderActivity;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_ACTION;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_ACTION_NOTIFY;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_ACTION_RING;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_TEXT;
import static com.usramm.nestor.firebase.NestorMessagingService.NESTOR_TITLE;


/**
 * Created by agnitsarkar on 5/20/17.
 */

public class NestorApplication extends MultiDexApplication {

    private static final String TAG = "ChatActivity";
    private boolean activityVisible = false;

    public class NestorBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            handleNestorReply(intent);
        }
    }

    NestorBroadCastReceiver nestorBroadCastReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        SystemManager.initSystemManager(this);
        NestorPreferences.initPreferences(this);

        nestorBroadCastReceiver = new NestorBroadCastReceiver();
        registerReceiver(nestorBroadCastReceiver, new IntentFilter(NestorMessagingService.BROADCAST_ACTION));
    }

    public boolean isActivityVisible() {
        return activityVisible;
    }

    public void activityResumed() {
        activityVisible = true;
    }

    public void activityPaused() {
        activityVisible = false;
    }

    private boolean handleNestorReply(Intent intent) {
        String text = intent.getStringExtra(NESTOR_TEXT);
        String title = intent.getStringExtra(NESTOR_TITLE);
        String action = intent.getStringExtra(NESTOR_ACTION);
        boolean validMessage = false;
        if (text != null && !text.isEmpty()
                && title != null && !title.isEmpty()
                && action != null && !action.isEmpty()) {
            validMessage = true;

            if (!isActivityVisible()) {
                if (action.equals(NESTOR_ACTION_NOTIFY)) {
                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        r.play();
                    } catch (Exception e) {
                        Log.d(TAG, e.getMessage());
                    }

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(this)
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setContentTitle(title)
                                    .setContentText(text)
                                    .setAutoCancel(true);

                    PendingIntent contentIntent = PendingIntent.getActivity(
                            this,
                            0,
                            new Intent(this, ChatActivity.class),
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
                    mBuilder.setContentIntent(contentIntent);

                    // show notification
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(12345, mBuilder.build());
                } else if (action.equals(NESTOR_ACTION_RING)){
                    /*
                    intent.setClass(NestorApplication.this, ReminderActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);

                    PendingIntent contentIntent = PendingIntent.getActivity(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(this)
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setContentTitle(title)
                                    .setContentText(text)
                                    .setAutoCancel(true);
                    mBuilder.setFullScreenIntent(contentIntent, true);

                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(12345, mBuilder.build());*/


                    intent.setClass(this, ReminderActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }
            } else {
                intent.setClass(NestorApplication.this, ChatActivity.class);
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

        return validMessage;
    }
}
